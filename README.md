This Project Contains Linquip font icons with IcoMoon.io configurations

1. Import selection.json into [IcoMoon](https://icomoon.io) website and make your changes
2. Export the project from IcoMoon (don't touch it's configs)
3. Replace exported files into this project
4. Make a commit from new changes and push it

Files will be uploaded to Amazon S3 Bucket using Gitlab CI/CD

The S3 files will be accessible using CloudFlare

URL structure will be like:
https://d1oihcihpfamzl.cloudfront.net/SHORT_COMMIT_CODE/

For example:
https://d1oihcihpfamzl.cloudfront.net/SHORT_COMMIT_CODE/style.css

5. Replace the new style.css URL in the _document.js in the main project